# README #

Questa applicazione si pone l'obbiettivo di ricreare un sito con funzionalit� simili a Youtube.
Utilizza algoritmi di natural language processing per:

1. identificare se un messaggio � da considerarsi spam o meno  
2. nel caso non sia spam per calcolare quanto � positivo il contenuto del messaggio che viene trasmesso  
3. per effettuare ricerche per tag  
4. per effettuare ricerche basate sulla combinazione di titolo del video, nome del canale e tag.

### Descrizione delle cartelle ###
- file_dati: cartella con file csv contenenti i dataset grezzi scaricati dai repository online
- pagine_html: cartella con versioni base delle pagine html poi implementate in flask
- progetto: la cartella con il progetto
- test: cartella con i test sulle varie funzionalit� eseguiti su jupyter
- struttura.txt: file con spiegazioni molto veloci sulla struttura delle tabelle base scaricate dai repository

### Come far partire il sistema ###
1. creare un ambiente virtuale ed installare i pacchetti numpy, pandas, tweepy, nltk, sklearn, scipy, flask
2. se non sono presenti i file nella cartella file_dati scaricare i file usando i link presenti nella relazione oppure nel file struttura.txt
3. eseguire il programma preparaDati.py per preparare i dati per l'applicazione
4. settare le variabili d'ambiente: su Windows set FLASK_APP=your_videos; set FLASK_DEBUG=true 
5. far partire flask con python -m flask run