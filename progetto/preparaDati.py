import numpy as np
import pandas as pd

GET_FILE_PATH = "../file_dati/"
PUT_FILE_PATH = "./your_videos/resource_file/"

#prepare spam file
spam_total_1 = pd.read_csv(GET_FILE_PATH + "Youtube01-Psy.csv")
spam_total_2 = pd.read_csv(GET_FILE_PATH + "Youtube02-KatyPerry.csv")
spam_total_3 = pd.read_csv(GET_FILE_PATH + "Youtube03-LMFAO.csv")
spam_total_4 = pd.read_csv(GET_FILE_PATH + "Youtube04-Eminem.csv")
spam_total_5 = pd.read_csv(GET_FILE_PATH + "Youtube05-Shakira.csv")
spam_total = pd.concat([spam_total_1, spam_total_2, spam_total_3, spam_total_4, spam_total_5], ignore_index=True)
spam_total = spam_total.drop(columns=['DATE', 'AUTHOR', 'COMMENT_ID'])
spam_total["label"] = np.where(spam_total["CLASS"] == 0 , "no spam", "spam")
spam_total = spam_total.drop(columns=['CLASS'])
spam_total.to_csv(PUT_FILE_PATH + "spam.csv", index=False)

#prepare comments file
data_comm_1 = pd.read_csv(GET_FILE_PATH + "GBcomments.csv", error_bad_lines=False)
data_comm_2 = pd.read_csv(GET_FILE_PATH + "UScomments.csv", error_bad_lines=False)
data_comm = pd.concat([data_comm_1, data_comm_2], ignore_index=True)
data_comm.drop_duplicates(subset=["video_id", "comment_text"], keep='last', inplace=True)
data_comm.to_csv(PUT_FILE_PATH + "youtube_comments.csv", index=False)

#prepare videos file
videoUS = pd.read_csv(GET_FILE_PATH + "USvideos.csv", error_bad_lines=False)
videoGB = pd.read_csv(GET_FILE_PATH + "GBvideos.csv", error_bad_lines=False)
video=pd.concat([videoGB, videoUS])
video.drop_duplicates(subset=["video_id"], keep='last', inplace=True)
video.to_csv(PUT_FILE_PATH + "youtube_videos.csv", index=False)

#prepare film reviews file
filmReview = pd.read_csv(GET_FILE_PATH + "imdb-review.csv", encoding = "ISO-8859-1", index_col=0)
filmReview = filmReview.drop(columns=['type', 'file'])
filmReview = filmReview[filmReview["label"] != 'unsup']
filmReview.to_csv(PUT_FILE_PATH + "imdb-review.csv", index=False)
