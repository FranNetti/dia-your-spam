from os import path

from flask import render_template, request, redirect, url_for, flash

import numpy as np
from . import app, spam, reviews, search
from .dataManager import DataManager

#load videos and comments datasets
video_manager = DataManager()

@app.route("/")
def home():
    n_rows = 2
    n_elem = 5
    videos = np.array(video_manager.getRandomVideo(n_elem * n_rows))
    videos = videos.reshape((n_rows, n_elem))
    return render_template("home.html", home_content=videos)

@app.route("/manage")
def manage():
    return render_template("manage.html")

@app.route("/video/<vid>")
def video(vid):
    current_vid = video_manager.getAllInfoOf(vid)
    other = video_manager.getRandomVideoOfCategory(current_vid.category, 10)
    #add if comments are considered spam or not
    current = spam.add_spam(current_vid)
    #add sentiment analysis to comments
    current = reviews.add_sent_analysis(current)
    return render_template("video.html", video=current, other_videos=other)

@app.route("/video")
def random_video():
    elem = video_manager.getRandomVideo(1)
    return redirect(url_for("video", vid=elem[0].link))

@app.route("/manage/evaluate", methods=["POST"])
def evaluate_sentence():
    text = request.form["text_rec"]
    res = spam.evaluate_sentence(text)
    if(res == ""): #if the system isn't trained on spam/not spam tell the user
        return render_template("manage.html", system_not_trained=True)
    if(res == "no spam"): #only if the result is not spam, analyse it
        pos = reviews.evaluate_sentence(text)
        if(pos != ""):
            res = "{} - {:.2f}% positivo".format(res, pos)
    return render_template("manage.html", eval_res=res)

@app.route("/search")
def video_search():
    text = request.args["search"]
    if text.startswith("#"): #tag search only if the sentence starts with #
        category = search.search_by_tag(text)
        if isinstance(category, str):
            flash("The search feature isn't trained yet, train it!")
            return redirect(url_for("manage"))
        videos = video_manager.getAllVideoOfCategory(category)
        return render_template("search.html", search_text=text, search_result=videos, category=category)
    else: #search by title
        search_res = search.complete_search(text)
        if isinstance(search_res, str):
            flash("The search feature isn't trained yet, train it!")
            return redirect(url_for("manage"))
        videos = video_manager.getAllVideoFrom(search_res.index)
        return render_template("search.html", search_text=text, search_result=videos)

@app.route("/train_spam", methods=["POST"])
def train_to_spam():
    spam.fit_spam()
    return redirect(url_for("manage"))

@app.route("/train_analysis", methods=["POST"])
def train_to_analysis():
    reviews.fit_analysis()
    return redirect(url_for("manage"))

@app.route("/trainTagSearch", methods=["POST"])
def enable_tag_search():
    #build tag index for tag search
    search.build_tag_index(video_manager.getAllVideos())
    flash("The system has now enabled the search feature by tag! You can now search easly in the search bar above!")
    return redirect(url_for("manage"))

@app.route("/trainFullSearch", methods=["POST"])
def enable_full_search():
    #build complete index for a complete title/channel/tag index
    search.build_complete_search_index(video_manager.getAllVideos())
    flash("The system has now enabled the search feature! You can now search easly in the search bar above!")
    return redirect(url_for("manage"))
