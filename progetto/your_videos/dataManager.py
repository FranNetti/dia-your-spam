import pandas as pd
import numpy as np

RESOURCE_FILE_PATH = "./your_videos/resource_file/"

def getSpamTrainingMessages():
    return pd.read_csv(RESOURCE_FILE_PATH + "spam.csv")

def getAnalysisTrainingReviews():
    return pd.read_csv(RESOURCE_FILE_PATH + "imdb-review.csv")

class DataManager:

    def __init__(self):
        self.data_comm = pd.read_csv(RESOURCE_FILE_PATH + "youtube_comments.csv")
        self.data_vid = pd.read_csv(RESOURCE_FILE_PATH + "youtube_videos.csv", index_col=0)

    def getAllInfoOf(self, vid):
        video = self.data_vid.loc[vid]
        video_merge = self.data_vid.loc[vid]
        comm = pd.merge(video_merge.to_frame().T, self.data_comm, left_index=True, right_on="video_id")
        comm = comm.loc[:, ["likes_y", "comment_text"]]
        comm.rename(index=str, columns={"likes_y": "likes", "comment_text": "comment"}, inplace=True)
        ret_video = Video(video, comm)
        return ret_video

    def getRandomVideo(self, number):
        video = self.data_vid.sample(n=number)
        list = []
        for v in video.index:
            list.append(Video(video.loc[v], None))
        return list

    def getAllVideos(self):
        return self.data_vid

    def getAllVideoOfCategory(self, cat):
        video = self.data_vid[self.data_vid["category_id"] == cat]
        list = []
        for v in video.index:
            list.append(Video(video.loc[v], None))
        return list

    def getRandomVideoOfCategory(self, cat, number):
        video = self.data_vid[self.data_vid["category_id"] == cat]
        video = video.sample(n=number)
        list = []
        for v in video.index:
            list.append(Video(video.loc[v], None))
        return list

    def getAllVideoFrom(self, index):
        video = self.data_vid.loc[index]
        list = []
        for v in range(0,50):
            list.append(Video(video.iloc[v], None))
        return list

class Video:

    def __init__(self, video, comments):
        self.author = video.channel_title
        self.title = video.title
        self.likes = video.likes
        self.dislikes = video.dislikes
        self.link = video.name
        self.views = video.views
        self.comments = comments
        self.category = video.category_id