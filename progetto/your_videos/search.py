import pickle
import nltk
import pandas as pd

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from flask import flash

INDEX_TAG_FILENAME = "./your_videos/train_file/tag_index.pkl"
INDEX_COMPLETE_SEARCH_FILENAME = "./your_videos/train_file/build_complete_index.pkl";

def tokenize_with_stemming(text):
    ps = nltk.stem.PorterStemmer()
    return [ps.stem(token) for token
            in nltk.tokenize.word_tokenize(text)]

def build_tag_index(videos):
    nltk.download("stopwords")
    stoplist = nltk.corpus.stopwords.words("english")
    videos = videos[videos.tags != "[none]"]
    model = Pipeline([
        ("vectorizer", TfidfVectorizer(stop_words=stoplist)),
        ("classifier", LogisticRegression(C=10))
    ])
    model.fit(videos.tags, videos.category_id)
    with open(INDEX_TAG_FILENAME, "wb") as f:
        pickle.dump(model, f)

def build_complete_search_index(videos):
    nltk.download("stopwords")
    stoplist = nltk.corpus.stopwords.words("english")
    videos = videos[videos.tags != "[none]"]
    model = Pipeline([
        ("vectorizer", TfidfVectorizer(ngram_range=(1,2), tokenizer=tokenize_with_stemming)),
        ("classifier", LogisticRegression(C=10))
    ])
    model.fit(videos.title + " " + videos.channel_title + " " + videos.tags, videos.index)
    with open(INDEX_COMPLETE_SEARCH_FILENAME, "wb") as f:
        pickle.dump(model, f)

def search_by_tag(sentence):
    #delete char that identifies tag search
    sentence = sentence[1:]
    try:
        with open(INDEX_TAG_FILENAME, "rb") as f:
            model = pickle.load(f)
        tag = model.predict([sentence])[0]
        return tag
    except FileNotFoundError as e:
        return ""

def complete_search(sentence):
    try:
        with open(INDEX_COMPLETE_SEARCH_FILENAME, "rb") as f:
            model = pickle.load(f)
        search = pd.DataFrame(model.predict_proba([sentence])).T
        search.set_index(model.classes_, inplace=True)
        search.sort_values(0,ascending=False, inplace=True)
        return search
    except FileNotFoundError as e:
        return ""
