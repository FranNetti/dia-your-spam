import pickle
from flask import flash
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline

from . import dataManager

CLASSIFIER_FILENAME = "./your_videos/train_file/spam_classifier.pkl"

def fit_spam():
    spam_train = dataManager.getSpamTrainingMessages()
    model = Pipeline([
        ("vectorizer", TfidfVectorizer(min_df=2, ngram_range=(1,2))),
        ("classifier", LogisticRegression(C=10))
    ])
    model.fit(spam_train.CONTENT, spam_train.label)
    with open(CLASSIFIER_FILENAME, "wb") as f:
        pickle.dump(model, f)
    flash("The system is now trained to recognize spam messages! You can now \
    go to the videos page and check out comments or \
    try the box to the right side with your personal message")

def add_spam(video):
    try:
        with open(CLASSIFIER_FILENAME, "rb") as f:
            model = pickle.load(f)
        spam_res = model.predict_proba(video.comments.comment)
        #add some balance for messages that could be considered spam
        video.comments["spam_res"] = np.where(spam_res[:,0] > 0.4, "no spam", "spam")
    except FileNotFoundError as e:
        pass
    return video

def evaluate_sentence(sentence):
    try:
        with open(CLASSIFIER_FILENAME, "rb") as f:
            model = pickle.load(f)
        spam_res = model.predict([sentence])
        return spam_res[0]
    except FileNotFoundError as e:
        return ""
