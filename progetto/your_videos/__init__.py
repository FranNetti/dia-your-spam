from flask import Flask
import requests

app = Flask(__name__)
app.config.from_mapping(
    SECRET_KEY="3f9rwhv0e8ht9c49y5444ytc9",
)

from . import pages
