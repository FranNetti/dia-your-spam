import pickle
import numpy as np
import pandas as pd
from flask import flash
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline


from . import dataManager

CLASSIFIER_FILENAME = "./your_videos/train_file/review_classifier.pkl"

def fit_analysis():
    reviews = dataManager.getAnalysisTrainingReviews()
    model = Pipeline([
        ("vectorizer", TfidfVectorizer(min_df=2, ngram_range=(1, 2))),
        ("classifier", LogisticRegression(C=10))
    ])
    model.fit(reviews.review, reviews.label)
    with open(CLASSIFIER_FILENAME, "wb") as f:
        pickle.dump(model, f)
    flash("The system is now trained for sentiment analysis! You can now \
    go to the videos page and check out comments or \
    try the box to the right side with your personal message.\
    You will see how much the message is positive only if it's not considered spam")

def add_sent_analysis(video):
    try:
        with open(CLASSIFIER_FILENAME, "rb") as f:
            model = pickle.load(f)
        sentiment = model.predict_proba(video.comments.comment)
        #take only the positive percentual
        video.comments["sent_res"] = sentiment[:, 1] * 100
    except FileNotFoundError as e:
        pass
    return video

def evaluate_sentence(sentence):
    try:
        with open(CLASSIFIER_FILENAME, "rb") as f:
            model = pickle.load(f)
        sent_res = model.predict_proba([sentence])[0,1] * 100
        return sent_res
    except FileNotFoundError as e:
        return ""
